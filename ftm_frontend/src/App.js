import React from 'react';
import CssBaseline from '@mui/material/CssBaseline';
import Container from '@mui/material/Container';
import Title from './Title';

const FamilyTreeView = React.lazy(() => import('ftm_treeview_mfe/FamilyTreeView'));
const Header = React.lazy(() => import('ftm_header_mfe/Header'));

const App = () => (
  <React.Fragment>
    <CssBaseline />
    <React.Suspense fallback="Loading header">
      <Header />
    </React.Suspense>
    <Container maxWidth="sm">
      <Title />
      <React.Suspense fallback="Loading family tree view">
        <FamilyTreeView />
      </React.Suspense>
    </Container>
  </React.Fragment>
);

export default App;
