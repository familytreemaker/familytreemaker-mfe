const { dependencies } = require('./package.json');

module.exports = {
  name: 'ftm_frontend',
  remotes: {
    ftm_treeview_mfe: 'ftm_treeview_mfe@http://localhost:3002/remoteEntry.js',
    ftm_header_mfe: 'ftm_header_mfe@http://localhost:3001/remoteEntry.js'
  },
  shared: {
    ...dependencies,
    react: {
      singleton: true,
      requiredVersion: dependencies['react'],
    },
    'react-dom': {
      singleton: true,
      requiredVersion: dependencies['react-dom'],
    },
  },
};
