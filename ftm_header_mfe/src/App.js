import Header from './Header';

const App = () => (
  <div>
    <Header />
  </div>
);

export default App;
