const { dependencies } = require('./package.json');

module.exports = {
  name: 'ftm_header_mfe',
  exposes: {
    './Header': './src/Header',
  },
  filename: 'remoteEntry.js',
  shared: {
    ...dependencies,
    react: {
      singleton: true,
      requiredVersion: dependencies['react'],
    },
    'react-dom': {
      singleton: true,
      requiredVersion: dependencies['react-dom'],
    },
  },
};
