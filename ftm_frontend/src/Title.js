import Box from '@mui/material/Box';

export default function Title() {
  return (
    <Box>
      <h1>
        Main container application
      </h1>
    </Box>
  )
}