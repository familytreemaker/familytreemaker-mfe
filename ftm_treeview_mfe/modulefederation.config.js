const { dependencies } = require('./package.json');

module.exports = {
  name: 'ftm_treeview_mfe',
  exposes: {
    './FamilyTreeView': './src/FamilyTreeView',
  },
  filename: 'remoteEntry.js',
  shared: {
    ...dependencies,
    react: {
      singleton: true,
      requiredVersion: dependencies['react'],
    },
    'react-dom': {
      singleton: true,
      requiredVersion: dependencies['react-dom'],
    },
  },
};
