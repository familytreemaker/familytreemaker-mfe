import FamilyTreeView from './FamilyTreeView';

const App = () => (
  <div>
    <h1>FamilyTreeViewer</h1>
    <FamilyTreeView />
  </div>
);

export default App;
