# Family Tree Maker Web App

This web app is based on microfrontend architecture.

Reference Github repo: https://github.com/module-federation/module-federation-examples/tree/master/cra

# How to use?

- Download this repo
- Change to this project's root directory
- Run `yarn`
- Run `yarn start`
- Check http://localhost:3000 -> This is the container
- Check http://localhost:3001 -> This is the header microfrontend
- Check http://localhost:3002 -> This is the tree view microfrontend

# FAQs

Q: What are microfrontends?

A: Good question. Check out https://martinfowler.com/articles/micro-frontends.html for a detailed answer.
   tldr; This is like microservices but for frontends. Each frontend team can develop their own component with CI/CD. Consumer teams can pick and choose modules for their overall container app. Development is much faster and website is less prone to errors.


Q: If this is MFE-based, then why do you have all modules in the same repo?

A: For simplicity. You can actually separate out these repos and each team can manage their own repo with CI/CD. If you see the frontend, you can see it's importing modules from a URL rather than from the directory